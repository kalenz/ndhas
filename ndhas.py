#! /usr/bin/env python3

import sys
import struct

from lexy import integer, string

from frasm.assembler import BaseAssembler
from frasm.data import BaseDataType
from frasm.opcode import BaseOpcodeType
from frasm.operand import RegisterOperand, ImmediateOperand, MemoryOperand
from frasm.entry import main

flag_opcodes = (
    'add',
    'addb',
    'addl',
    'and',
    'andb',
    'andl',
    'call',
    'cmp',
    'cmpb',
    'cmpl',
    'div',
    'divb',
    'divl',
    'mov',
    'movb',
    'movl',
    'mul',
    'mulb',
    'mull',
    'or',
    'orb',
    'orl',
    'push',
    'pushb',
    'pushl',
    'sub',
    'subb',
    'subl',
    'xor',
    'xorb',
    'xorl',
)

class Opcode(BaseOpcodeType):

    # Mapping operand size -> operand mask
    operand_masks = {
        8:  0xff,
        16: 0xffff,
    }

    # Mapping operand size -> binary structure
    operand_structs = {
        8:  struct.Struct('>B'),
        16: struct.Struct('>H'),
        32: struct.Struct('>I'),
    }

    def __init__(self, mnemonic, base_opcode, *args, **kwargs):
        #global flag_opcodes
        super(Opcode, self).__init__(mnemonic, *args, **kwargs)
        self.base_opcode = base_opcode
        self.has_flag = mnemonic in flag_opcodes
        self.size = 1 + sum(op_type.size // 8 for op_type in self.operands)
        self.size = self.size + 1 if self.has_flag else self.size

    def encode(self, *operands):
        if self.has_flag:
            opcode_bytes = [
                self.operand_structs[16].pack(self.base_opcode),
            ]
        else:
            opcode_bytes = [
                self.operand_structs[8].pack(self.base_opcode),
            ]

        for op_value, op_type, in zip(operands, self.operands):
            if op_type.ignored:
                continue
            op_value = op_value & self.operand_masks[op_type.size]
            opcode_bytes.append(
                self.operand_structs[op_type.size].pack(op_value)
            )
        return b''.join(opcode_bytes)

class ByteType(BaseDataType):

    def __init__(self):
        super(ByteType, self).__init__('byte', integer, size=8)

    def validate(self, byte):
        if byte < 0 or byte > 255:
            raise ValueError('Invalid byte: {}'.format(byte))

    def encode(self, byte):
        return bytes([byte])

class LongType(BaseDataType):

    def __init__(self):
        super(LongType, self).__init__('long', integer, size=16)

    def validate(self, byte):
        if byte < 0 or byte > 65535:
            raise ValueError('Invalid byte: {}'.format(byte))

    def encode(self, byte):
        return bytes([byte])

class StringType(BaseDataType):

    def __init__(self):
        super(StringType, self).__init__('string', string)

    def validate(self, string):
        pass

    def encode(self, string):
        # TODO: check if there is nothing more appropriate
        return string.encode('latin-1')

class Assembler(BaseAssembler):

    def locate_sections(self):
        code = self.sections['']
        code.set_address(0x8000)

    def write_binary(self, fp):
        code = self.sections['']
        fp.write(b'.NDH')
        fp.write(struct.Struct('<H').pack(code.size))
        code.write(fp)

byte_op = ImmediateOperand(size=8)
long_op = ImmediateOperand(size=16)

gpr = {'r{}'.format(i): i for i in '01234567'}
gpr.update({'sp' : 8, 'bp' : 9 })
reg_op = RegisterOperand(gpr, size=8)

opcodes = (
    # Stack
    Opcode('push',      0x0103, reg_op),
    Opcode('pushb',     0x0104, byte_op),
    Opcode('pushl',     0x0105, long_op),
    Opcode('pop',       0x03, reg_op),

    # Memory
    Opcode('mov',       0x0400, reg_op, reg_op),
    Opcode('mov',       0x040a, reg_op, MemoryOperand(reg_op)),
    Opcode('movb',      0x0401, reg_op, byte_op),
    Opcode('movl',      0x0402, reg_op, long_op),
    Opcode('mov',       0x0406, MemoryOperand(reg_op), reg_op),
    Opcode('mov',       0x0409, MemoryOperand(reg_op), MemoryOperand(reg_op)),
    Opcode('movb',      0x0407, MemoryOperand(reg_op), byte_op),
    Opcode('movl',      0x0408, MemoryOperand(reg_op), long_op),

    # Arithmetics
    Opcode('add',       0x0600, reg_op, reg_op),
    Opcode('addb',      0x0601, reg_op, byte_op),
    Opcode('addl',      0x0602, reg_op, long_op),
    Opcode('sub',       0x0700, reg_op, reg_op),
    Opcode('subb',      0x0701, reg_op, byte_op),
    Opcode('subl',      0x0702, reg_op, long_op),
    Opcode('mul',       0x0800, reg_op, reg_op),
    Opcode('mulb',      0x0801, reg_op, byte_op),
    Opcode('mull',      0x0802, reg_op, long_op),
    Opcode('div',       0x0900, reg_op, reg_op),
    Opcode('divb',      0x0901, reg_op, byte_op),
    Opcode('divl',      0x0902, reg_op, long_op),
    Opcode('inc',       0x0A, reg_op),
    Opcode('dec',       0x0B, reg_op),

    # Logic
    Opcode('or',        0x0C00, reg_op, reg_op),
    Opcode('orb',       0x0C01, reg_op, byte_op),
    Opcode('orl',       0x0C02, reg_op, long_op),
    Opcode('and',       0x0D00, reg_op, reg_op),
    Opcode('andb',      0x0D01, reg_op, byte_op),
    Opcode('andl',      0x0D02, reg_op, long_op),
    Opcode('xor',       0x0E00, reg_op, reg_op),
    Opcode('xorb',      0x0E01, reg_op, byte_op),
    Opcode('xorl',      0x0E02, reg_op, long_op),
    Opcode('not',       0x0F, reg_op),

    # Control
    Opcode('jz',        0x10, long_op),
    Opcode('jnz',       0x11, long_op),
    Opcode('jmps',      0x16, byte_op),
    Opcode('jmpl',      0x1B, long_op),
    Opcode('test',      0x17, reg_op, reg_op),
    Opcode('cmp',       0x1800, reg_op, reg_op),
    Opcode('cmpb',      0x1801, reg_op, byte_op),
    Opcode('cmpl',      0x1802, reg_op, long_op),
    Opcode('call',      0x1904, long_op),
    Opcode('call',      0x1903, reg_op),
    Opcode('ret',       0x1A),
    Opcode('end',       0x1C),
    Opcode('xchg',      0x1D, reg_op, reg_op),
    Opcode('ja',        0x1E, long_op),
    Opcode('jb',        0x1F, long_op),

    # Misc
    Opcode('syscall',   0x30),
    Opcode('nop',       0x02),
)

assembler = Assembler(
    sections = (),
    data_types = (ByteType(), LongType(), StringType()),
    opcodes = opcodes,

    warning_lazy_opcode_size = True,
)

if __name__ == '__main__':
    main(assembler, sys.argv[1:])
