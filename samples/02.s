MOVB    R0, #0x2
MOVL    R1, :filename
MOVB    R2, #0x0
SYSCALL         ; open(filename, O_RDONLY)
MOV     R7, R0

.label :loop
MOVB    R0, #0x03
MOV     R1, R7
MOV     R2, SP
MOVB    R3, #0x01
SYSCALL         ; read(fd, sp, 1)
TEST    R0, R0
JNZ     :read_ok
END

.label :read_ok
MOVB    R0, #0x04
MOVB    R1, #0x01
MOV     R2, SP
MOVB    R3, #0x01
SYSCALL         ; write(stdout, sp, 1)
JMPS    :loop

.label :filename
.ascii "/proc/self/cmdline"
